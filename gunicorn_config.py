import os

bind = "unix:bmbc-web.sock"
workers = 3

if os.path.exists('/etc/bmbc-web/bmbc-web.conf'):
    with open('/etc/bmbc-web/bmbc-web.conf', 'r') as conf:
        raw_env = conf.readlines()
