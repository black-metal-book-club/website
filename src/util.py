
import re
import json
import math

from enum import IntEnum
from dataclasses import dataclass, InitVar, field, asdict

from dateutil import tz
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta

import boto3
import botocore

from flask_caching import Cache
from flask import Flask

from redis.exceptions import OutOfMemoryError


APP_TIMEZONE = tz.gettz('America/New_York')


class Dates:
    KEY_FORMAT = '%Y%m%d'

    def yesterday() -> datetime:
        return datetime.now(APP_TIMEZONE) - timedelta(days=1)
    
    def today() -> datetime:
        return datetime.now(APP_TIMEZONE)
    
    def tomorrow() -> datetime:
        return datetime.now(APP_TIMEZONE) + timedelta(days=1)
    
    def day_after_tomorrow() -> datetime:
        return datetime.now(APP_TIMEZONE) + timedelta(days=2)
    
    @classmethod
    def yesterday_key(cls) -> str:
        return cls.yesterday().strftime(cls.KEY_FORMAT)
    
    @classmethod
    def today_key(cls) -> str:
        return cls.today().strftime(cls.KEY_FORMAT)
    
    @classmethod
    def tomorrow_key(cls) -> str:
        return cls.tomorrow().strftime(cls.KEY_FORMAT)

    @classmethod
    def day_after_tomorrow_key(cls) -> str:
        return cls.day_after_tomorrow().strftime(cls.KEY_FORMAT)


class CacheKey:
    COUNTS_DAY = 'bmbc-counts:day'
    COUNTS_MONTH = 'bmbc-counts:month'
    RELEASES_DAY = 'bmbc-releases:day'
    RELEASES_MONTH = 'bmbc-releases:month'
    AVAILABLE_MIN = 'bmbc-dates:min'
    AVAILABLE_MAX = 'bmbc-dates:max'
    BLOB_PREFIX = 'bmbc/web/releases'

    @classmethod
    def live_keys(cls) -> tuple[str, str, str, str]:
        return [cls.COUNTS_DAY + ':' + Dates.yesterday_key(),
                cls.COUNTS_DAY + ':' + Dates.today_key(),
                cls.COUNTS_DAY + ':' + Dates.tomorrow_key(),
                cls.COUNTS_DAY + ':' + Dates.day_after_tomorrow_key()]

    @classmethod
    def create_counts_day_key(cls, date_key: str) -> str:
        return cls.COUNTS_DAY + ':' + date_key

    @classmethod
    def create_counts_month_key(cls, month_key: str) -> str:
        return cls.COUNTS_MONTH + ':' + month_key

    @classmethod
    def create_releases_day_key(cls, day_key: str) -> str:
        return cls.RELEASES_DAY + ':' + day_key

    @classmethod
    def create_releases_month_key(cls, month_key: str) -> str:
        return cls.RELEASES_MONTH + ':' + month_key
    
    @staticmethod
    def create_releases_blob_key(day_key: str) -> str:
        return f'bmbc/web/releases/date={day_key}.json'
    
    @classmethod
    def month_key_to_date_keys(cls, key: str) -> list[str]:
        key = key.split(':')[-1]

        if len(key) == 6:
            return [key]

        start_date = datetime.strptime(key, '%Y%m')
        end_date = cls.next_month_start(start_date) - timedelta(days=1)

        days = (end_date - start_date).days
        days = map(lambda n: end_date - timedelta(days=n), range(days))
        days = map(lambda n: n.strftime('%Y%m%d'), days)
        return list(days)


    @staticmethod
    def next_month_start(date: datetime) -> datetime:
        if date.day > 1:
            month = date.month + 1
            year = date.year
            if month == 13:
                month = 1
                year += 1

            return datetime(year, month, 1)
        else:
            return date

    @staticmethod
    def prior_month_end(date: datetime) -> datetime:
        return datetime(date.year, date.month, 1) - timedelta(days=1)

    @classmethod
    def date_range_to_keys(cls, start_date: datetime, end_date: datetime) -> list[tuple[str, str, str]]:
        date_delta = relativedelta(end_date, start_date)

        month_difference = (date_delta.years * 12) + date_delta.months
        
        if month_difference > 1:
            month_lower_bound = cls.next_month_start(start_date)
            month_upper_bound = cls.prior_month_end(end_date)
            
            month_bound_delta = relativedelta(month_upper_bound, month_lower_bound)

            months = (month_bound_delta.years * 12) + month_bound_delta.months
            months = map(lambda n: (int(n / 12), n % 12), range(months + 1))
            months = map(lambda n: relativedelta(years=n[0], months=n[1]), months)
            months = map(lambda n: month_upper_bound - n, months)
            months = sorted(list(set(map(lambda n: n.strftime('%Y%m'), months))), reverse=True)
            month_keys = map(cls.create_releases_month_key, months)
            month_counts = map(cls.create_counts_month_key, months)

            spare_days_prior = (month_lower_bound - start_date).days + 1
            spare_days_prior = map(lambda n: month_lower_bound - timedelta(days=n), range(1, spare_days_prior))
            spare_days_prior = list(map(lambda n: n.strftime('%Y%m%d'), spare_days_prior))
            
            days_prior_keys = map(cls.create_releases_day_key, spare_days_prior)
            days_prior_counts = map(cls.create_counts_day_key, spare_days_prior)
            days_prior_blobs = map(cls.create_releases_blob_key, spare_days_prior)

            spare_days_after = (end_date - month_upper_bound).days
            spare_days_after = map(lambda n: end_date - timedelta(days=n), range(spare_days_after))
            spare_days_after = list(map(lambda n: n.strftime('%Y%m%d'), spare_days_after))

            days_after_keys = map(cls.create_releases_day_key, spare_days_after)
            days_after_counts = map(cls.create_counts_day_key, spare_days_after)
            days_after_blobs = map(cls.create_releases_blob_key, spare_days_after)

            months = list(zip([None] * len(months), month_keys, month_counts))
            prior = list(zip(days_prior_blobs, days_prior_keys, days_prior_counts))            
            after = list(zip(days_after_blobs, days_after_keys, days_after_counts))

            return after + months + prior
        
        else:
            days = (end_date - start_date).days + 1
            days = map(lambda n: end_date - timedelta(days=n), range(days))
            days = list(map(lambda n: n.strftime('%Y%m%d'), days))
            day_keys = map(cls.create_releases_day_key, days)
            count_keys = map(cls.create_counts_day_key, days)
            blob_keys = map(cls.create_releases_blob_key, days)

            return list(zip(blob_keys, day_keys, count_keys))


class PageSize(IntEnum):
    XSMALL = 33
    SMALL = 66
    LARGE = 99
    XLARGE = 333
    XXLARGE = 666
    XXXLARGE = 999

    @classmethod
    def to_list(cls) -> list[int]:
        return [n.value for n in cls]


class Regex:
    DATE = re.compile((r'^(19|20)[0-9][0-9]' # year
                       r'\-(0[0-9]|1[0-2])'  # month
                       r'\-(0[0-9]|1[0-9]|2[0-9]|3[0-1])$')) # day


class Functions:
    
    @classmethod
    def filter_releases_by_date(cls, dataset: list[dict], date: str) -> dict:
        def filter_clause(release):
            return release['release_date'] == date

        return {date: sorted(list(filter(filter_clause, dataset)), key=cls.sort_key)}
    
    @classmethod
    def filter_releases_by_date_range(cls, dataset: list[dict], date_range: set) -> dict:
        categorized_dataset = {date: list() for date in sorted(list(date_range), reverse=True)}

        for record in dataset:
            categorized_dataset[record['release_date']].append(record)

        final_dataset = [{'date': k, 'records': v} for k, v in categorized_dataset.items() if len(v) > 0]

        return final_dataset
    

@dataclass
class PagePicker:
    total_records: InitVar[int]
    
    page_size: int
    current_page: int
    next_page: int = field(init=False)
    last_page: int = field(init=False)

    first: str = field(init=False)
    back_five: str = field(init=False)
    back_one: str = field(init=False)
    next_one: str = field(init=False)
    next_five: str = field(init=False)
    last: str = field(init=False)

    def __post_init__(self, total_records):
        page_size = self.page_size
        current_page = self.current_page

        self.last_page = last_page = math.ceil(total_records / page_size)
        self.next_page = next_page = min(current_page + 1, last_page)

        self.first = f'?page_size={page_size}&page=1'
        self.back_five = f'?page_size={page_size}&page={max(current_page - 5, 1)}'
        self.back_one = f'?page_size={page_size}&page={max(current_page - 1, 1)}'
        self.next_one = f'?page_size={page_size}&page={next_page}'
        self.next_five = f'?page_size={page_size}&page={min(current_page + 5, last_page)}'
        self.last = f'?page_size={page_size}&page={last_page}'


@dataclass
class PageDates:
    available_dates_range: InitVar[tuple[str, str]]
    selected_dates_range: InitVar[tuple[str, str]]
    
    available_min: str = field(init=False)
    available_max: str = field(init=False)

    selected_min: str = field(init=False)
    selected_max: str = field(init=False)

    def __post_init__(self, available_range, selected_range):
        self.available_min, self.available_max = available_range
        self.selected_min, self.selected_max = selected_range


@dataclass
class PageResults:
    page_size: InitVar[int] = field(kw_only=True)
    current_page: InitVar[int] = field(kw_only=True)
    total_records: int = field(kw_only=True)
    raw_data: InitVar[list[dict]] = field(kw_only=True)

    available_dates_range: InitVar[tuple[datetime, datetime]] = field(kw_only=True)
    selected_dates_range: InitVar[tuple[datetime, datetime]] = field(kw_only=True)

    available_page_sizes: list[int] = field(default_factory=PageSize.to_list)

    data: list[dict] = field(init=False)
    links: PagePicker = field(init=False)
    dates: PageDates = field(init=False)

    def __post_init__(self, page_size: int, current_page: int, data: list[dict], 
                      available_dates_range: tuple[str, str], 
                      selected_dates_range: tuple[str, str]) -> dict:
        
        unique_dates = set(map(lambda n: n['release_date'], data))
        self.data = Functions.filter_releases_by_date_range(data, unique_dates)
        self.links = PagePicker(self.total_records, page_size, current_page)
        self.dates = PageDates(available_dates_range, selected_dates_range)

    def to_dict(self) -> dict:
        return asdict(self)
        
    def to_json(self):
        dataset = dict(total=self.total_records, count=len(self.data), data=self.data)

        if self.links.current_page < self.links.last_page:
            next_endpoint = (f'/api/releases'
                            f'?start_date={self.dates.selected_min}'
                            f'&end_date={self.dates.selected_max}'
                            f'&page_size={self.links.page_size}'
                            f'&page={self.links.next_page}')

            dataset['next'] = next_endpoint
        
        return dataset


class Context:
    app: Flask
    cache: Cache

    def __init__(self, name, app: Flask | None = None):
        self.app = app = app if app else Flask(name)
        
        app.config.from_object('src.config.RedisConfig')
        app.config.from_object('src.config.SpacesConfig')

        self.cache = Cache(app)

        self.bucket = app.config['SPACES_BUCKET']
        self.session = boto3.session.Session()
        self.config = botocore.config.Config(s3=dict(addressing_style='virtual'))

        client_kwargs = dict(config=self.config, region_name='nyc3',
                             endpoint_url=app.config['SPACES_ENDPOINT'],
                             aws_access_key_id=app.config['SPACES_ACCESS_KEY'],
                             aws_secret_access_key=app.config['SPACES_SECRET_KEY'])

        self.client = self.session.client('s3', **client_kwargs)

    def get_available_date_range_from_blob(self) -> tuple[str | None, str | None]:
        bmbc_blobs = list()
        bucket = self.bucket

        response_metadata = self.client.list_objects_v2(Bucket=bucket, Prefix='bmbc/sink/releases')
        bmbc_blobs += response_metadata['Contents']
        
        while 'NextContinuationToken' in response_metadata:
            response_metadata = self.client.list_objects_v2(Bucket=bucket, Prefix='bmbc/sink/releases',
                                                            ContinuationToken=response_metadata['NextContinuationToken'])
            bmbc_blobs += response_metadata['Contents']

        bmbc_blobs = [blob['Key'] for blob in bmbc_blobs]
        bmbc_blobs = [b.split('.') for b in bmbc_blobs]
        bmbc_blobs = [b[0].split('/')[-1] for b in bmbc_blobs if b[-1] == 'json']
        bmbc_blobs = [int(b) for b in bmbc_blobs if b != '00000000']

        return min(bmbc_blobs), max(bmbc_blobs)

    def get_available_date_range_from_cache(self) -> tuple[str | None, str | None]:
        cached_min = self.cache.get(CacheKey.AVAILABLE_MIN)
        cached_max = self.cache.get(CacheKey.AVAILABLE_MAX)
        
        try:
            max_timestamp = float(cached_max)
            max_date = datetime.fromtimestamp(max_timestamp)

        except TypeError:
            max_date = None

        if datetime.now().date() < max_date.date():
            try:
                min_timestamp = float(cached_min)
                min_date = datetime.fromtimestamp(min_timestamp)
            except TypeError:
                min_date = None
        else:
            min_date, max_date = self.get_available_date_range_from_blob()

        return min_date, max_date
    
    @property
    def current_month_date_key(self) -> int:
        return int(datetime.now(APP_TIMEZONE).strftime('%Y%m'))
    
    @property
    def current_month_cache_key(self) -> str:
        date_str = datetime.now(APP_TIMEZONE).strftime('%Y%m')
        return CacheKey.create_releases_month_key(date_str)

    @property
    def todays_date_key(self) -> int:
        return int(datetime.now(APP_TIMEZONE).strftime('%Y%m%d'))

    @property
    def todays_blob_key(self) -> str:
        date_str = datetime.now(APP_TIMEZONE).strftime('%Y%m%d')
        return CacheKey.create_releases_blob_key(date_str)
    
    @property
    def todays_data_key(self) -> str:
        date_str = datetime.now(APP_TIMEZONE).strftime('%Y%m%d')
        return CacheKey.create_releases_day_key(date_str)
    
    def get_releases_object(self, date_key: str) -> dict:
        response = self.client.list_objects_v2(Bucket=self.bucket, Prefix=date_key, MaxKeys=1)
        return response['Contents'][0]
    
    def get_releases_metadata(self, date_key: str) -> dict:
        streaming_body = self.client.get_object(Bucket=self.bucket, Key=date_key)
        return streaming_body['Metadata']
    
    def get_record_count(self, count_key: str, blob_key: str) -> int:

        if self.cache.has(count_key):
            record_count = self.cache.get(count_key)
        
        else:
            metadata = self.get_releases_metadata(blob_key)
            record_count = int(metadata['x-amz-meta-record-count'])
            
            timeout = 600 if blob_key in CacheKey.live_keys() else 0
            try:
                self.cache.set(count_key, record_count, timeout=timeout)
            except OutOfMemoryError:
                self.app.logger.info('redis cache out of memory')

        return record_count

    def get_releases(self, data_key: str, blob_key: str | None) -> dict:

        is_month_key = blob_key is None

        if self.cache.has(data_key):
            # if the dataset is already cached
            
            records = json.loads(self.cache.get(data_key)).get('data', [])

        elif is_month_key:
            # if the dataset is not cached, and is for a full month's worth

            records = list()
            for date_key in CacheKey.month_key_to_date_keys(data_key):
                day_blob_key = CacheKey.create_releases_blob_key(date_key)
                streaming_body = self.client.get_object(Bucket=self.bucket, Key=day_blob_key)
                data = streaming_body['Body'].read()
                data = json.loads(data).get('data', [])
                records += data

            timeout = 600 if data_key == self.current_month_cache_key else 0

            try:
                self.cache.set(data_key, json.dumps(dict(data=records)), timeout=timeout)

            except OutOfMemoryError:
                self.app.logger.info('redis cache out of memory')

        else:
            # if the requested dataset is not cached, and is less than a month's worth
            
            streaming_body = self.client.get_object(Bucket=self.bucket, Key=blob_key)
            data = streaming_body['Body'].read()
            records = json.loads(data).get('data', [])

            timeout = 600 if data_key in CacheKey.live_keys() else 0

            try:
                self.cache.set(data_key, json.dumps(dict(data=records)), timeout=timeout)

            except OutOfMemoryError:
                self.app.logger.info('redis cache out of memory')

        return list(sorted(records, key=lambda n: (n['band'], n['album'])))

    def get_releases_page(self, start_date: str, end_date: None | str, 
                          page_size: int, page: int) -> PageResults:
            
        start_date_obj = datetime.strptime(start_date, '%Y-%m-%d')

        if end_date is None:
            end_date = start_date

        end_date_obj = datetime.strptime(end_date, '%Y-%m-%d')
        
        keys = CacheKey.date_range_to_keys(start_date_obj, end_date_obj)
        
        records = list()
        
        lower_bound = (page - 1) * page_size
        upper_bound = (page_size * page)
        record_count = 0
        
        for blob_key, data_key, count_key in keys:
            subset_count = self.get_record_count(count_key, blob_key)
            record_count += subset_count

            if lower_bound > record_count:
                records += [{'release_date': '9999-99-99'}] * subset_count
            elif upper_bound > record_count:
                records += self.get_releases(data_key, blob_key)
            elif record_count > upper_bound and upper_bound > len(records):
                records += self.get_releases(data_key, blob_key)

        records = sorted(records, key=lambda n: n['release_date'], reverse=True)
        dataset = records[lower_bound:upper_bound]

        available_dates_range = self.get_available_date_range_from_cache()

        return PageResults(page_size=page_size, current_page=page, 
                           total_records=record_count, raw_data=dataset, 
                           selected_dates_range=(start_date, end_date), 
                           available_dates_range=available_dates_range)
