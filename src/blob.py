
import json
import boto3
import boto3.session
import botocore

import botocore.config
from flask.config import Config


class BlobKey:
    RELEASE_DATES = 'bmbc/model/release'
    
    @staticmethod
    def album(band_id: str, album_id: str) -> str:
        return f'bmbc/model/album/band={band_id}/album={album_id}.json'
    
    @staticmethod
    def albums(band_id: str) -> str:
        return f'bmbc/model/album/band={band_id}'

    @staticmethod
    def band(band_id: str) -> str:
        return f'bmbc/model/band/band={band_id}.json'

    @staticmethod
    def label(label_id: str) -> str:
        return f'bmbc/model/label/label={label_id}.json'

    @staticmethod
    def release_objects(release_date: str) -> str:
        return f'bmbc/model/release/date={release_date}'

    @staticmethod
    def release(release_date: str, album_key: str, band_key: str) -> str:
        return f'bmbc/model/release/date={release_date}/album={album_key}/band={band_key}'
    
    @classmethod
    def month(cls, year: int, month: int) -> str:
        return f'bmbc/model/release/date={year}{str(month).zfill(2)}'
    
    @classmethod
    def year(cls, year: int) -> str:
        return f'bmbc/model/release/date={year}'


class BlobStorage:
    bucket: str

    def __init__(self, app_config: Config):
        self.bucket = app_config['SPACES_BUCKET']
        self.session = session = boto3.session.Session()
        self.config = botocore.config.Config(s3=dict(addressing_style='virtual'))
        self.client = session.client('s3', config=self.config, 
                                     region_name=app_config['SPACES_REGION'],
                                     endpoint_url=app_config['SPACES_ENDPOINT'],
                                     aws_access_key_id=app_config['SPACES_ACCESS_KEY'],
                                     aws_secret_access_key=app_config['SPACES_SECRET_KEY'])
        
    def list_objects_by_prefix(self, prefix: str) -> list[str]:
        bucket = self.bucket
        response = self.client.list_objects_v2(Bucket=bucket, Prefix=prefix)
        try:
            blobs = [blob['Key'] for blob in response['Contents']]
        except KeyError:
            return []

        while 'NextContinuationToken' in response:
            continuation_token = response['NextContinuationToken']
            response = self.client.list_objects_v2(ContinuationToken=continuation_token,
                                                   Bucket=bucket, Prefix=prefix)

            blobs += [blob['Key'] for blob in response['Contents']]

        return blobs
    
    def get_object_by_key(self, key: str) -> dict: 
        response = self.client.get_object(Bucket=self.bucket, Key=key)
        dataset = json.load(response['Body'])
        return dataset['data']
    
    def parse_key_parameters(self, key: str, prefix: str | None = None) -> dict:
        key_params = dict()

        if prefix is None:
            for key_value_str in key.split('/'):
                if '=' not in key_value_str:
                    continue
                
                param_key, param_value = key_value_str.split('=', maxsplit=1)
                key_params[param_key] = param_value
        elif prefix is not None:
            prefix = '/'.join([segment for segment in prefix.split('/') 
                               if '=' not in segment and segment != ''])

            key = key.replace(prefix, '')
            for key_value_str in key.split('/'):
                if '=' not in key_value_str:
                    continue

                param_key, param_value = key_value_str.split('=', maxsplit=1)
                key_params[param_key] = param_value

        return key_params
