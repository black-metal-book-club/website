

from flask_caching import Cache

__all__ = ['Cache']


class CacheKey:
    RELEASE_DATES = 'api:releases:dates'

    @staticmethod
    def album(band_id: str, album_id: str) -> str:
        return f'api:album:{band_id}:{album_id}'
    
    @staticmethod
    def albums(band_id: str) -> str:
        return f'api:album:{band_id}:all'

    @staticmethod
    def band(band_id: str) -> str:
        return f'api:band:{band_id}'

    @staticmethod
    def label(label_id: str) -> str:
        return f'api:label:{label_id}'

    @staticmethod
    def releases(release_date: str) -> str:
        return f'api:releases:{release_date}'
    
    @staticmethod
    def month(year: int, month: int) -> str:
        return f'api:releases:{year}{str(month).zfill(2)}'
    
    @staticmethod
    def year(year: int) -> str:
        return f'api:releases:{year}'
