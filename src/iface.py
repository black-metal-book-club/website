from itertools import starmap
from datetime import datetime, date
from dataclasses import dataclass, asdict


@dataclass
class AvailableDate:
    available_date: str
    count: int

    def month(self) -> date:
        date_obj = datetime.strptime(self.available_date, '%Y-%m-%')
        return date(date_obj.year, date_obj.month, 1)
    
    def __dict__(self):
        return dict(date=self.available_date, count=self.count)


@dataclass
class AvailableDates:
    dates: list[AvailableDate]

    def to_dict(self):
        return asdict(self)
    
    @classmethod
    def from_list_of_tuples(cls, list_: list[tuple]):
        return cls(list(starmap(AvailableDate, list_)))
    
    @classmethod
    def from_dict(cls, dict_: dict[str, list[dict]]):
        return cls.from_list_of_dicts(dict_['dates'])
    
    @classmethod
    def from_list_of_dicts(cls, list_: list[dict[str, str | int]]):
        return cls(list(map(lambda n: AvailableDate(str(n['available_date']), int(n['count'])), list_)))

    @classmethod
    def from_count_dict(cls, dict_: dict[str, int]):
        return cls(list(starmap(AvailableDate, dict_.items())))

