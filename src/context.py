import json
# from datetime import datetime
from logging import Logger

from flask import Flask

# from redis.exceptions import OutOfMemoryError

# from .iface import AvailableDates
# from .blob import BlobStorage, BlobKey
# from .cache import Cache, CacheKey
# from .calendar import ReleaseCalendar


# class KeyManager:
#     blob: BlobKey
#     cache: CacheKey

#     def __init__(self):
#         self.blob = BlobKey()
#         self.cache = CacheKey()


class Context:
    app: Flask
    # cache: Cache
    # blob: BlobStorage
    # key: KeyManager

    def __init__(self, name, app: Flask | None = None):
        self.app = app = app if app else Flask(name)
        # self.key = KeyManager()

        if not isinstance(app.logger, Logger):
            raise Exception
            
        # app.config.from_object('src.config.RedisConfig')
        # app.config.from_object('src.config.SpacesConfig')

        # self.cache = Cache(app)
        # self.blob = BlobStorage(app.config)

    @property
    def logger(self) -> Logger:
        logger = self.app.logger
        if not isinstance(logger, Logger):
            raise ValueError
        return logger
    
    # def set_object(self, cache_key: str, obj: dict, timeout=0) -> None:
    #     try:
    #         self.cache.set(cache_key, json.dumps(obj), timeout=timeout)

    #     except OutOfMemoryError:
    #         self.logger.info('redis cache out of memory')
    #         self.logger.error(f'unable to store {cache_key}')

    # def get_album(self, band_id: str, album_id: str) -> dict:
    #     cache_key = self.key.cache.album(band_id, album_id)
    #     if self.cache.has(cache_key):
    #         album = json.loads(str(self.cache.get(cache_key)))

    #     else:
    #         blob_key = self.key.blob.album(band_id, album_id)
    #         album = self.blob.get_object_by_key(blob_key)
    #         self.set_object(cache_key, album)

    #     return album
    
    # def get_band(self, band_id: str) -> dict:
    #     cache_key = self.key.cache.band(band_id)

    #     if self.cache.has(cache_key):
    #         band = json.loads(str(self.cache.get(cache_key)))
    #     else:
    #         blob_key = self.key.blob.band(band_id)
    #         band = self.blob.get_object_by_key(blob_key)
    #         self.set_object(cache_key, band)

    #     return band
    
    # def get_label(self, label_id: str) -> dict:
    #     cache_key = self.key.cache.label(label_id)

    #     if self.cache.has(cache_key):
    #         label = json.loads(str(self.cache.get(cache_key)))
    #     else:
    #         blob_key = self.key.blob.label(label_id)
    #         label = self.blob.get_object_by_key(blob_key)
    #         self.set_object(cache_key, label)

    #     return label

    # def list_albums(self, band_id: str) -> dict:
    #     cache_key = self.key.cache.albums(band_id)

    #     if self.cache.has(cache_key):
    #         albums = json.loads(str(self.cache.get(cache_key)))

    #     else:
    #         blob_key = self.key.blob.albums(band_id)
    #         blob_client = self.blob
            
    #         albums_list = list()
    #         for album_key in blob_client.list_objects_by_prefix(blob_key):
    #             albums_list.append(blob_client.get_object_by_key(album_key))

    #         albums = dict(band_id=band_id, albums=albums_list)
    #         self.set_object(cache_key, albums)
            
    #     return albums

    # def list_release_dates(self) -> dict:
    #     cache_key = self.key.cache.RELEASE_DATES

    #     if self.cache.has(cache_key):
    #         release_dates = json.loads(str(self.cache.get(cache_key)))

    #     else:
    #         blob_key = self.key.blob.RELEASE_DATES
    #         blob_client = self.blob

    #         object_keys = blob_client.list_objects_by_prefix(blob_key)
    #         object_params = map(lambda n: blob_client.parse_key_parameters(n, blob_key), object_keys)

    #         date_counts = dict()
    #         for date in object_params:
    #             try:
    #                 date_counts[date['date']] += 1
    #             except KeyError:
    #                 date_counts[date['date']] = 0

    #         release_dates = [dict(date=d, releases=r) for d, r in date_counts.items()]
    #         release_dates = dict(dates=release_dates)

    #         self.set_object(cache_key, release_dates, timeout=600)

    #     return release_dates
    
    # def list_release_dates_by_month(self, year: int, month: int) -> AvailableDates:
    #     cache_key = self.key.cache.month(year, month)
        
    #     if self.cache.has(cache_key):
    #         release_dates = json.loads(str(self.cache.get(cache_key)))
    #         release_dates = AvailableDates.from_list_of_dicts(release_dates['dates'])

    #     else:
    #         blob_key = self.key.blob.month(year, month)
    #         blob_client = self.blob

    #         object_keys = blob_client.list_objects_by_prefix(blob_key)
    #         object_params = map(lambda n: blob_client.parse_key_parameters(n, blob_key), object_keys)

    #         date_counts = dict()
    #         for date in object_params:
    #             try:
    #                 date_counts[date['date']] += 1
    #             except KeyError:
    #                 date_counts[date['date']] = 0

    #         release_dates = AvailableDates.from_count_dict(date_counts)
    #         release_dates_dict = release_dates.to_dict()

    #         self.set_object(cache_key, release_dates_dict, timeout=600)

    #     return release_dates
    
    # def list_release_dates_by_year(self, year: int) -> AvailableDates:
    #     cache_key = self.key.cache.year(year)
        
    #     if self.cache.has(cache_key):
    #         release_dates_dict = json.loads(str(self.cache.get(cache_key)))
    #         release_dates = AvailableDates.from_dict(release_dates_dict)
    #     else:
    #         blob_key = self.key.blob.year(year)
    #         blob_client = self.blob

    #         object_keys = blob_client.list_objects_by_prefix(blob_key)
    #         object_params = map(lambda n: blob_client.parse_key_parameters(n, blob_key), object_keys)

    #         date_counts = dict()
    #         for date in object_params:
    #             try:
    #                 date_counts[date['date']] += 1
    #             except KeyError:
    #                 date_counts[date['date']] = 1

    #         release_dates = AvailableDates.from_list_of_tuples(list(date_counts.items()))

    #         self.set_object(cache_key, release_dates.to_dict(), timeout=600)

    #     return release_dates
    
    # def list_releases(self, release_date: str) -> dict:
    #     cache_key = self.key.cache.releases(release_date)
        
    #     if self.cache.has(cache_key):
    #         release_objects = json.loads(str(self.cache.get(cache_key)))
        
    #     else:
    #         blob_key = self.key.blob.release_objects(release_date)
    #         blob_client = self.blob
    #         release_keys = blob_client.list_objects_by_prefix(blob_key)
    #         release_objects = list(map(blob_client.get_object_by_key, release_keys))

    #         release_date_display = datetime.strptime(release_date, '%Y%m%d').strftime('%Y-%m-%d')
    #         release_objects = dict(date=release_date_display, releases=release_objects)
            
    #         self.set_object(cache_key, release_objects)

    #     return release_objects
    
    # def list_releases_by_month(self, year: str, month: str) -> list[dict]:
    #     list_releases = self.list_releases
    #     year_int, month_int = int(year), int(month)
    #     available_dates = self.list_release_dates_by_month(year_int, month_int)
    #     dates = available_dates.dates
    #     dataset = list(map(lambda n: list_releases(n.available_date), dates))
    #     return dataset

    # def get_month_calendar(self, month_key: str) -> str:
    #     year = int(month_key[:4])
    #     month = int(month_key[4:])
    #     available_dates = self.list_release_dates_by_month(year, month)
    #     return ReleaseCalendar(available_dates, firstweekday=7).formatmonth(year, month, f'/releases/calendar/{year}/{month}')

    # def get_year_calendar(self, year_key: str) -> str:
    #     available_dates = self.list_release_dates_by_year(int(year_key))
    #     return ReleaseCalendar(available_dates, firstweekday=7).formatyear(int(year_key), f'/releases/calendar/{year_key}')
