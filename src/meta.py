
from datetime import datetime


SUPPORTED_FORMATS = ['json', 'csv', 'xlsx', 'yaml']


class Meta:

    @staticmethod
    def current_time():
        return datetime.now()
    

    @classmethod
    def create_metadata(cls, **kwargs):
        current_time = cls.current_time()
        return dict(year=str(current_time.year),
                    month=str(current_time.month).zfill(2),
                    day=str(current_time.day).zfill(2),
                    supported_formats=SUPPORTED_FORMATS,
                    **kwargs)