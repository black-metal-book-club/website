
import os
from datetime import datetime
from calendar import HTMLCalendar, month_name

from .iface import AvailableDates

JANUARY = 1


class ReleaseCalendar(HTMLCalendar):
    current_date: datetime
    available_dates: dict[str, int]
    
    def __init__(self, available_dates: AvailableDates, firstweekday: int = 0):
        super().__init__(firstweekday=firstweekday)
        self.current_date = datetime.now()
        self.available_dates = {record.available_date: record.count 
                                for record in available_dates.dates}

    def formatday(self, day: int, weekday: int, endpoint: str = '/'):
        """
        Return a day as a table cell.
        """
        if day == 0:
            # day outside month
            return f'<td class="{self.cssclass_noday}">&nbsp;</td>'
        else:
            day_str = str(day)
            endpoint_split = endpoint.split('/')
            year = endpoint_split[-2]
            month = endpoint_split[-1]
            day_key = year + month.zfill(2) + day_str.zfill(2)

            if self.available_dates.get(day_key, 0) > 0:
                day_endpoint = os.path.join(endpoint, day_str)

                release_count = self.available_dates[day_key]
                day_str += f'</ br><div><a href="{day_endpoint}">{release_count}</a></div>'
            else:
                day_str += '</ br><div> - </div>'

            return f'<td class="{self.cssclasses[weekday]}">{day_str}</td>'

    def formatweek(self, theweek, endpoint: str = '/'):
        """
        Return a complete week as a table row.
        """
        week_str = ''.join(self.formatday(d, wd, endpoint) for (d, wd) in theweek)
        return f'<tr>{week_str}</tr>'
    
    def formatmonth(self, theyear, themonth, endpoint: str = '/', withyear=True):
        """
        Return a formatted month as a table.
        """
        lines = [f'<table class="{self.cssclass_month}">',
                 self.formatmonthname(theyear, themonth, endpoint=endpoint, withyear=withyear),
                 self.formatweekheader()]
        
        append = lines.append
        for week in self.monthdays2calendar(theyear, themonth):
            append(self.formatweek(week, endpoint=endpoint))
        
        append('</table>')
        
        return '\n'.join(lines) + '\n'
    
    def formatmonthname(self, theyear, themonth, endpoint: str = '/', withyear=True):
        """
        Return a month name as a table row.
        """
        if withyear:
            month_str = f'{month_name[themonth]} {theyear}'
        else:
            month_str = month_name[themonth]

        month_anchor = f'<a href="{endpoint}">{month_str}</a>'
        return f'<caption class="{self.cssclass_month_head}">{month_anchor}</caption>'
    
    def formatyear(self, theyear, endpoint: str = '/'):
        """
        Return a formatted year as a table of tables.
        """
        
        lines = [f'<div class="{self.cssclass_year}">',
                 f'<h3 class="{self.cssclass_year_head}">{theyear}</th>']
        
        append = lines.append
        
        for month in range(JANUARY, JANUARY + 12, 1):
            month_date = datetime(theyear, month, 1, 0, 0, 0, 0)
            if month_date > self.current_date:
                break
            
            append('<div>')
            
            month_endpoint = os.path.join(endpoint, str(month))
            month_str = self.formatmonth(theyear, month, endpoint=month_endpoint, withyear=False)
            append(month_str)

            append('</div>')
        append('</section>')

        return '\n'.join(lines)