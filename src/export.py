import tablib


class Export:
    @staticmethod
    def load_data(dataset: list[dict]) -> tablib.Dataset:
        table = tablib.Dataset()
        headers = set()

        for record in dataset:
            for key in record:
                headers.add(key)

        columns = {header: [] for header in headers}
        for record in dataset:
            for header in headers:
                value = record.get(header, None)
                columns[header].append(value)

        for header, column in columns.items():
            table.append_col(column, header)

        return table
