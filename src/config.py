import os


class RedisConfig:
    CACHE_TYPE = 'RedisCache'
    CACHE_KEY_PREFIX = ''
    CACHE_REDIS_HOST = os.environ['CACHE_REDIS_HOST']
    CACHE_REDIS_PORT = os.environ['CACHE_REDIS_PORT']
    CACHE_REDIS_DB = os.environ['CACHE_REDIS_DB']
    CACHE_REDIS_PASSWORD = os.environ['CACHE_REDIS_PASSWORD']
    CACHE_REDIS_USE_TLS = os.environ['CACHE_REDIS_USE_TLS']
    CACHE_REDIS_PROTOCOL = 'rediss' if bool(int(CACHE_REDIS_USE_TLS)) else 'redis'
    CACHE_REDIS_URL = (f'{CACHE_REDIS_PROTOCOL}://default:{CACHE_REDIS_PASSWORD}'
                       f'@{CACHE_REDIS_HOST}:{CACHE_REDIS_PORT}/{CACHE_REDIS_DB}')
    CACHE_DEFAULT_TIMEOUT = os.environ['CACHE_DEFAULT_TIMEOUT']

class SpacesConfig:
    SPACES_REGION = os.environ['SPACES_REGION']
    SPACES_BUCKET = os.environ['SPACES_BUCKET']
    SPACES_ENDPOINT = os.environ['SPACES_ENDPOINT']
    SPACES_ACCESS_KEY = os.environ['SPACES_ACCESS_KEY']
    SPACES_SECRET_KEY = os.environ['SPACES_SECRET_KEY']
