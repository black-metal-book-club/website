
document.getElementById('form-release-date').addEventListener('submit', (event) => {
    let startDate = document.getElementById('select-release-date-start').value;
    let endDate = document.getElementById('select-release-date-end').value;
    if (startDate && !endDate) {
        window.open(`/releases/${startDate}`, '_self');
    }
    else if (startDate && endDate) {
        window.open(`/releases/${startDate}/${endDate}`, '_self');
    }
    else {
        window.open(`/releases`, '_self');
    }
});
