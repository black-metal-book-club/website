# import io
import os
# from datetime import datetime

from flask import (redirect, render_template, send_file)

from src import Context  # , Metadata, Export

context = Context(__name__)

def get_app():
    return context.app

DEBUG = bool(int(context.app.config['DEBUG']))


# @context.app.route('/api/band/<band_id>/album/<album_id>')
# def api_album(band_id: str, album_id: str):
#     return context.get_album(band_id, album_id)


# @context.app.route('/api/band/<band_id>/albums')
# def api_albums(band_id: str):
#     return context.list_albums(band_id)


# @context.app.route('/api/band/<band_id>')
# def api_band(band_id: str):
#     return context.get_band(band_id)


# @context.app.route('/api/label/<label_id>')
# def api_label(label_id: str):
#     return context.get_label(label_id)


# @context.app.route('/api/releases')
# def api_releases():
#     return context.list_release_dates()


# @context.app.route('/api/releases/<release_date>')
# def api_releases_by_date(release_date: str):
#     return context.list_releases(release_date)


# @context.app.route('/api/releases/<release_year>/<release_month>/<export_format>')
# def api_releases_report_by_month_formatted(release_year: str, release_month: str, export_format: str):
#     dataset = context.list_releases_by_month(release_year, release_month)
#     records = list(sum([date['releases'] for date in dataset], []))
#     export = Export.load_data(records).export(export_format)

#     filename = f'release-{release_year}{release_month.zfill(2)}.{export_format}'
#     if isinstance(export, str):
#         export_buffer = io.BytesIO(export.encode())
#     else:
#         export_buffer = io.BytesIO(export)

#     return send_file(export_buffer, attachment_filename=filename, mimetype=export_format, as_attachment=True)


# @context.app.route('/api/releases/<release_year>/<release_month>')
# def api_releases_report(release_year: str, release_month: str):
#     return dict(data=context.list_releases_by_month(release_year, release_month))


@context.app.route('/')
def root():
    return render_template('index.jinja')


@context.app.route('/home')
def home():
    return redirect('/')


@context.app.route('/favicon.ico')
def favicon():
    favicon_path = os.path.join(str(context.app.root_path), 'static/ico/favicon.ico')
    return send_file(favicon_path, mimetype='image/vnd.microsoft.icon')


# @context.app.route('/band/<band_id>')
# def band(band_id: str):
#     return render_template('band.jinja', data=context.get_band(band_id))


# @context.app.route('/band/<band_id>/album/<album_id>')
# def album(band_id: str, album_id: str):
#     data = context.get_album(band_id, album_id)
#     return render_template('album.jinja', data=data)


# @context.app.route('/label/<label_id>')
# def label(label_id: str):
#     return render_template('label.jinja', data=context.get_label(label_id))


# @context.app.route('/releases/calendar')
# def calendar():
#     date = datetime.now()
#     return redirect(f'/releases/calendar/{date.year}/{date.month}')


# @context.app.route('/releases/calendar/<year>')
# def calendar_year(year: str):
#     calendar = context.get_year_calendar(year)
#     meta = Metadata.create_metadata()
#     return render_template('releases-calendar.jinja', calendar=calendar, meta=meta)


# @context.app.route('/releases/calendar/<year>/<month>')
# def calendar_month(year: str, month: str):
#     calendar = context.get_month_calendar(year + month.zfill(2))
#     meta = Metadata.create_metadata()
#     return render_template('releases-calendar.jinja', calendar=calendar, meta=meta)


# @context.app.route('/releases/calendar/<year>/<month>/<day>')
# def calendar_day(year: str, month: str, day: str):
#     return redirect(f'/releases/{year}/{month}/{day}')


# @context.app.route('/releases')
# def releases():
#     date = datetime.now()
#     return redirect(f'/releases/{date.year}/{date.month}/{date.day}')


# @context.app.route('/releases/<year>/<month>/<day>')
# @context.cache.cached(query_string=True, unless=lambda: DEBUG)
# def releases_by_day(year: str, month: str, day: str):
#     padded_month = month.zfill(2) 
#     padded_day = day.zfill(2)

#     date_str = year + '-' + padded_month + '-' + padded_day
#     meta = Metadata.create_metadata(selected_date=date_str)

#     date_key = year + padded_month + padded_day
#     data = context.list_releases(date_key)

#     return render_template('releases-list.jinja', data=data, meta=meta)


# @context.app.route('/releases/<date_key>')
# @context.cache.cached(query_string=True, unless=lambda: DEBUG)
# def releases_by_date_key(date_key: str):
#     date = datetime.strptime(date_key, '%Y%m%d')
#     return redirect(f'/releases/{date.year}/{date.month}/{date.day}')


# @context.app.route('/releases/<start_date>/<end_date>')
# @context.cache.cached(query_string=True, unless=lambda: DEBUG)
# def releases_by_date_range(start_date: str, end_date: str):
#     return render_template('v1/releases.jinja')
